#include <string>
#include <iostream>

class Player {
public:
	Player(int id, std::string nm, float r) : number(id), name(nm), rating(r) {}
	void print_info() {
		std::cout << "Player id: " << number << std::endl;
		std::cout << "Player name: " << name << std::endl;
		std::cout << "Player rating: " << rating << std::endl;
	}
private:
	int number;
	std::string name;
	float rating;
};
#include <iostream>
#include "Player.h"
#include "Vector.h"

int main()
{
    std::cout << "1. My class 'Player':\n";
    Player player(1, "Andrey", 10);
    player.print_info();
    std::cout << std::endl;

    std::cout << "2. Class 'Vector':\n";
    Vector vector(2, 5, 7);
    std::cout << "Vector coordinates: ";
    vector.Show();
    std::cout << "\nVector length: " << vector.GetLength() << std::endl;
}

